<div class="max-w-2xl mx-auto p-4 sm:p-6 lg:p-8">
    <h1>Simple Form</h1>

    <form method="POST" action="{{ route('simpleform.forms.store') }}">
            @csrf
            <textarea
                name="message"
                placeholder="{{ __('What\'s on your mind?') }}"
                class="block w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
            >{{ old('message') }}</textarea>
            <x-primary-button class="mt-4">{{ __('Save Message') }}</x-primary-button>
    </form>

    <br/><br/><br/><br/>

    <div class="mt-6 bg-white shadow-sm rounded-lg divide-y">

        @foreach ($forms as $form)

	    <span class="text-gray-800">{{ $form->message }}</span>
	    <br/><br/>

	@endforeach

    </div>

</div>
