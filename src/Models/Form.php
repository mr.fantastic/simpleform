<?php

namespace Mrfantastic\Simpleform\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Form extends Model
{
    use HasFactory;

    protected $fillable = [
        'message',
    ];

    // protected $dispatchesEvents = [
    //     'created' => FormCreated::class,
    // ];
}
