<?php

namespace Mrfantastic\Simpleform\Http\Controllers;

use Mrfantastic\Simpleform\Models\Form;

use Illuminate\Routing\Controller;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class FormController extends Controller
{
 
    public function index()
    {
        //return 'It works!';
        return view('simpleform::forms.index',[
            'forms' => Form::all(),
        ]);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) : RedirectResponse
    {
        $validated = $request->validate([
            'message' => 'required|string|max:255',
        ]);
        Form::create($validated);
        // return redirect(route('forms.index'));
        //return 'It works!';
        return redirect()->route('simpleform.forms.index');
    }
 
}
