<?php

namespace Mrfantastic\Simpleform;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
 
class SimpleformServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }
 
    public function boot()
    {
        Route::prefix('simpleform')
            ->as('simpleform.')
            ->group(function () {
                $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
            });

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'simpleform');

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations/');


        $this->publishes([
            __DIR__.'/../database/migrations/create_forms_table.php.stub' =>
            database_path('migrations/' . date('Y_m_d_His', time()) . '_create_forms_table.php')
        ], 'migrations');
    }
}
