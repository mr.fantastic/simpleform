<?php

use Mrfantastic\Simpleform\Http\Controllers\FormController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

Route::resource('forms', FormController::class)
    ->only(['index', 'store']);
